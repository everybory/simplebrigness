package simplebrigness;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;
import simplebrigness.util.Configuration;

/**
 *
 * @author ORTIZ
 */
public class PersistData {

    private static final String PATH = Configuration.getRutaRelativa();

    public static synchronized void serializeModel(DefaultTableModel model, String fileName) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(path() + fileName + ".txt"), "utf-8"))) {
            for (int i = 0; i < model.getRowCount(); i++) {
                for (int ii = 0; ii < model.getColumnCount(); ii++) {
                    writer.append(model.getValueAt(i, ii).toString() + ";");
                }
                writer.append("\n");
            }
            writer.close();
        } catch (IOException i) {
            //Logger.error(i);
        }
    }

    public static synchronized void desSerielizeModel(DefaultTableModel model, JTable table, String fileName) {
        String cadena;
        FileReader f = null;
        try {
            f = new FileReader(path() + fileName + ".txt");
        } catch (FileNotFoundException ex) {
            //Logger.error(ex);
        }
        if (f != null) {
            try {
                model.setRowCount(0);
                BufferedReader b = new BufferedReader(f);
                while ((cadena = b.readLine()) != null) {
                    Object[] data = new Object[model.getColumnCount()];
                    data = cadena.split(";");
                    model.addRow(data);
                }
                table.setModel(model);
            } catch (IOException ex) {
                //Logger.error(ex);
            }
            try {
                f.close();
            } catch (IOException ex) {
                //Logger.error(ex);
            }
        }
    }

    public static synchronized void save(String text, String fileName) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(path() + fileName + ".txt"), "utf-8"))) {
            writer.write(text);
            writer.close();
        } catch (IOException i) {
            //Logger.error(i);
        }
    }

    public static synchronized void load(JTextComponent component, String fileName) {
        String cadena;
        String text = "";
        FileReader f = null;
        try {
            f = new FileReader(path() + fileName + ".txt");
        } catch (FileNotFoundException ex) {
            //Logger.error(ex);
        }
        if (f != null) {
            try {
                BufferedReader b = new BufferedReader(f);
                int lines = 0;
                while ((cadena = b.readLine()) != null) {
                    text += cadena +"\n";
                    lines++;
                }
                if(lines==1){
                    text = text.trim();
                }
                component.setText(text);
            } catch (IOException ex) {
                //Logger.error(ex);
            }
            try {
                f.close();
            } catch (IOException ex) {
                //Logger.error(ex);
            }
        }
    }
    
    public static synchronized String getText(String fileName) {
        String cadena;
        String text = "";
        FileReader f = null;
        try {
            f = new FileReader(path() + fileName + ".txt");
        } catch (FileNotFoundException ex) {
            //Logger.error(ex);
        }
        if (f != null) {
            try {
                BufferedReader b = new BufferedReader(f);
                while ((cadena = b.readLine()) != null) {
                    text += cadena;
                }
            } catch (IOException ex) {
                //Logger.error(ex);
            }
            try {
                f.close();
            } catch (IOException ex) {
                //Logger.error(ex);
            }
        }
        return text;
    }

    private static String path() {
        new File(PATH).mkdir();
        return PATH;
    }
    
}
